# Object Detection App

## Overview

This is an Object Detection App that uses Flask and Detectron2 to process uploaded MP4 videos. The app performs object detection on each frame of the video and returns the list of objects with their probabilities. It's designed to be scalable and uses multithreading to handle multiple video files.

## Features

- Supports MP4 video format
- Uses Detectron2 for object detection
- Multi-threaded for better performance
- Limits video processing to clips up to 5 seconds long
- Written in Python 3.11

## Installation

First, clone the repository:

```bash
git clone https://gitlab.com/Napruishkin/python_flask_test_task.git
```


### Virtual Environment

It's recommended to use a virtual environment to manage dependencies:

```bash
python3 -m venv venv
source venv/bin/activate  # On Windows, use `venv\Scripts\activate`
```

### Dependencies

Install the required packages:

```bash
pip install -r requirements.txt
```

### Running the App

To run the app, execute:

```bash
python app.py
```

The app should be running on `http://127.0.0.1:5000/`.

## Usage

1. Open your web browser and navigate to `http://127.0.0.1:5000/`.
2. Upload an MP4 video using the file input.
3. The app will process the video and return the detected objects and their probabilities for each frame.

## Architecture Design

1. **Front-end**: HTML file with file input for uploading videos.
2. **Back-end**: Flask app that handles video uploads and starts a new thread for each video to perform object detection.
3. **Object Detection**: Uses Detectron2 library to process video frames.

## Scaling
For scaling, consider using a message broker like RabbitMQ and workers that process videos from a queue.

