import os
import threading
from typing import Union
from flask import Flask, request, render_template, Response
import cv2

from object_detection import process_video  # Assuming process_video is a function in a module called object_detection

app = Flask(__name__)


def allowed_file(filename: str) -> bool:
    """Check if the file has a valid extension.

    Args:
        filename (str): The name of the file.

    Returns:
        bool: True if valid, False otherwise.
    """
    return '.' in filename and filename.rsplit('.', 1)[1].lower() == 'mp4'


@app.route('/', methods=['GET', 'POST'])
def upload_video() -> Union[str, Response]:
    """Handles the video upload and starts the processing.

    Returns:
        Union[str, Response]: A string message or rendered HTML template.
    """
    if request.method == 'POST':
        video_file = request.files.get('video')

        if video_file and allowed_file(video_file.filename):
            video_path = os.path.join("uploads", video_file.filename)
            video_file.save(video_path)

            # Using OpenCV to get video properties
            video = cv2.VideoCapture(video_path)
            fps = video.get(cv2.CAP_PROP_FPS)
            frame_count = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
            duration = frame_count / fps

            # Validate video duration
            if duration <= 5:
                # Starting a new thread to process the video
                threading.Thread(target=process_video, args=(video,)).start()
            else:
                return "Video should be up to 5 seconds long."
        else:
            return "Invalid file format. Please upload an MP4 video."

    return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=True)
