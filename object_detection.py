from typing import List, Dict, Any
import cv2

# Detectron2 imports
from detectron2 import model_zoo
from detectron2.config import get_cfg
from detectron2.engine import DefaultPredictor
from detectron2.utils.logger import setup_logger

# Setup logger for Detectron2
setup_logger()


def process_video(video: cv2.VideoCapture) -> List[Dict[Any, Any]]:
    """
    Processes a video to identify objects in each frame.

    Args:
        video (cv2.VideoCapture): The video object.

    Returns:
        List[Dict[Any, Any]]: List of dictionaries, where each dictionary contains
        the labels and their corresponding probabilities for each frame.
    """

    # Configure the model
    cfg = get_cfg()
    cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5
    cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")

    # Create a predictor instance with the model configuration
    predictor = DefaultPredictor(cfg)

    frames_data = []  # List to hold data for each frame

    # Read and process each frame
    while True:
        ret, frame = video.read()

        # Break if the video has ended
        if not ret:
            break

        # Make prediction on the current frame
        outputs = predictor(frame)

        frame_data = {}  # Dictionary to hold labels and their corresponding scores for the current frame

        # Populate the frame_data dictionary
        for label, score in zip(outputs["instances"].pred_classes, outputs["instances"].scores):
            frame_data[label.item()] = score.item()

        frames_data.append(frame_data)

    # Release the video capture object
    video.release()

    return frames_data
